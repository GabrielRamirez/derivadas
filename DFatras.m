function df=DFatras(d,g,f,x,h)
% d es para elegir si de primera diferencia o segunda
%la g grado de la derivada si primera,segunda,tercera o cuarta.
alto=0;
fx=inline(f);
while alto == 0
   switch o
       case 1
           %Primera diferencia
           if r==1
                   D1=(fx(x)-fx(x-h))/h;
                  df= D1;
           else
               if r==2
                   D2=(fx(x)-2*fx(x-h)+fx(x-2*h))/h^2;
                   df=D2;
               else
                   if r==3
                   D3=(fx(x)-3*fx(x-h)+3*fx(x-2*h)-fx(x-3*h))/h^3;
                  df= D3;
                   else
                       if r==4
                       D4=(fx(x)-4*fx(x-h)+6*fx(x-2*h)-4*fx(x-3*h)+fx(x-4*h))/h^4;
                      df= D4;
                       end  
                   end
               end
           end
           
           alto=1;
       case 2
        %Segunda diferencia
           if r==1
               d1=((3*fx(x))-(4*fx(x-h))+(fx(x-2*h)))/(2*h);
               df=d1;
           else
               if r==2
                   d2=(2*fx(x)-5*fx(x-h)+4*fx(x-2*h)-fx(x-3*h))/h^2;
                  df= d2;
               else
                   if r==3
                       d3=(5*fx(x)-18*fx(x-h)+24*fx(x-2*h)-14*fx(x-3*h)+3*fx(x-4*h))/h^3;
                       df=d3;
                   else if r==4
                           d4=(3*f(x)-14*fx(x-h)+26*fx(x-2*h)-24*fx(x-3*h)+11*fx(x-4*h)-2*fx(x-5*h))/h^4;
                          df= d4;
                       end
                   end
               end
           end
           alto=1;
       otherwise
           alto =1;
           end
end
