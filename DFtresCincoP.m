function df=DFtresCincoP(d,g,f,x,h)
%la d es para elegir si de cinco puntos o de tres
%la g para elegir si primera o segunda diferencia.
alto=0;
fx=inline(f);
while alto == 0
   switch o
       case 1
           %tres untos    
           if r==1
                   D1=(1/(2*h))*(-(3*fx(x))+(4*fx(x+h))-fx(x+2*h));
                  df= D1;
           end
           alto=1;
       case 2
           %cinco puntos
           if r==1
               %primera diferencia
               d1=(1/(12*h))*(-25*fx(x)+48*fx(x+h)-36*fx(x+2*h)+16*fx(x+3*h)-3*fx(x+4*h));
              df= d1;
           else
               if r==2
                   %segunda diferencia
                   d2=(1/(2*h))*(fx(x-2*h)-8*fx(x-h)+8*fx(x+h)-fx(x+2*h));
                  df= d2;
               
               end
           end
           alto=1;
       otherwise
           alto =1;
           end
end
