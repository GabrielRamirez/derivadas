function df=DFcentrales(d,g,f,x,h)
% d elegir si la primera diferencia o segunda
%la g grdo de la derivada si primera,segunda,tercera o cuarta.
alto=0;
fx=inline(f);
while alto == 0
   switch o
       case 1
           %Primera diferencia
           if r==1
                   D1=(fx(x+h)-fx(x-h))/(2*h);
                  df= D1;
           else
               if r==2
                   D2=(fx(x+h)-2*fx(x)+fx(x-h))/h^2;
                   df=D2;
               else
                   if r==3
                   D3=(fx(x+2*h)-2*fx(x+h)+2*fx(x-h)-fx(x-2*h))/(2*(h^3));
                  df= D3;
                   else
                       if r==4
                       D4=(fx(x+2*h)-4*fx(x+h)+6*fx(x)-4*fx(x-h)+fx(x-2*h))/h^4;
                       df=D4;
                       end  
                   end
               end
           end
           
           alto=1;
       case 2
           %Segunda diferencia
        
           if r==1
               d1=(-(fx(x+2*h))+(8*fx(x+h))-8*fx(x-h)+fx(x-2*h))/(12*h);
              df= d1;
           else
               if r==2
                   d2=(-fx(x+2*h)+16*fx(x+h)-30*fx(x)+16*fx(x-h)-fx(x-2*h))/(12*(h^2));
                 df=  d2;
               else
                   if r==3
                       d3=(-fx(x+3*h)+8*fx(x+2*h)-12*fx(x+h)+12*fx(x-h)-8*fx(x-2*h)+fx(x-3*h))/(8*(h^3));
                       df=d3;
                   else if r==4
                           d4=(-fx(x+3*h)+12*fx(x+2*h)-39*fx(x+h)+56*fx(x)-39*fx(x-h)+12*fx(x-2*h)-fx(x-3*h))/(6*(h^4));
                           df=d4;
                       end
                   end
               end
           end
           alto=1;
       otherwise
           alto =1;
           end
end
